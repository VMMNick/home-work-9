function createListFromArray(array, parent = document.body) {
  const ul = document.createElement('ul');
  array.forEach(item => {
    const li = document.createElement('li');
    li.textContent = item; 
    ul.appendChild(li);
  });
  parent.appendChild(ul);
}
const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const array2 = ["1", "2", "3", "sea", "user", 23];
createListFromArray(array1);
createListFromArray(array2, document.getElementById('customParent')); // Прикріпиться до елемента з id "customParent"
